package com.task.mytodo.activity

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.task.mytodo.R
import com.task.mytodo.adapter.TodoListAdapter
import com.task.mytodo.api.ApiInterface
import com.task.mytodo.api.RetrofitApiClient
import com.task.mytodo.databinding.ActivityHomeBinding
import com.task.mytodo.model.RemoveTaskRequest
import com.task.mytodo.model.TaskData
import com.task.mytodo.model.TaskListRequest
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class HomeActivity : BaseActivity(), TodoListAdapter.OnClickListener {
    private lateinit var binding: ActivityHomeBinding
    var taskData : ArrayList<TaskData> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
    override fun onResume() {
        super.onResume()
        init()
    }

    private fun init() {
        //toolbar
        binding.toolbar.btnBack.isVisible = false
        binding.toolbar.activityMainToolbarTitle.text = "Home"

        //add task click listener
        binding.addToDoList.setOnClickListener {
            var intent = Intent(this, TaskAddActivity::class.java)
            startActivity(intent)
        }

        //task List Api
        taskListAPICall()
    }

    private fun taskListAPICall() {
        val returnPopUp = isNetworkAvailable()
        hideSoftKeyBoard(this)
        if (!returnPopUp) {
            Toast.makeText(this, "check your internet connection!", Toast.LENGTH_SHORT).show()
            return
        }else{
            showLoader()
            disposable = RetrofitApiClient.createService("", ApiInterface::class.java)
                .callAddTaskApi(
                    TaskListRequest(
                        "1",
                    )
                )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ response ->
                    dismissLoader()
                    val taskListResponse = response.body()
                    if (response.code() == 401) {
                        responseStatus()
                    }else{
                        if (taskListResponse?.status == 200) {
                            var message = taskListResponse.message
                            taskData = taskListResponse.data.tasks
                            updateRecyclerView(0)
                        } else {
                            val message = taskListResponse?.message
                            dialogStatus(message!!,10)
                        }
                    }
                }, {
                    dismissLoader()
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                })
        }
    }


    private fun updateRecyclerView(position: Int) {
        try {
            binding.todoListRecycler.isNestedScrollingEnabled = false
            binding.todoListRecycler.layoutManager = LinearLayoutManager(this)
            binding.todoListRecycler.adapter = TodoListAdapter(taskData,this,this)
            binding.todoListRecycler.scrollToPosition(position)
        } catch (e: Exception) {
            Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onItemClick(position: Int,clickType: Int,taskid: Int) {
        if (clickType==2) {
            deleteDialog(position,taskid)
        }
    }

    private fun deleteDialog(position:Int,taskid: Int) {
        var myDialog = Dialog(this)
        myDialog.setContentView(R.layout.delete_dialog_alert)
        myDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        myDialog.show()

        var btnNo = myDialog.findViewById<TextView>(R.id.btnNo)
        var btnYes = myDialog.findViewById<TextView>(R.id.btnYes)

        btnYes.setOnClickListener {
            myDialog.dismiss()
            taskRemoveAPICall(taskid,position)
        }
        btnNo.setOnClickListener {
            myDialog.dismiss()
        }

        myDialog.setCanceledOnTouchOutside(true)
        myDialog.setCancelable(false)
        myDialog.setOnDismissListener {
            myDialog.dismiss()
        }
    }


    //task remove api call
    private fun taskRemoveAPICall(taskid: Int,arrayPosition: Int) {
        val returnPopUp = isNetworkAvailable()
        hideSoftKeyBoard(this)
        if (!returnPopUp) {
            Toast.makeText(this, "check your internet connection!", Toast.LENGTH_SHORT).show()
            return
        }else{
            showLoader()
            disposable = RetrofitApiClient.createService("", ApiInterface::class.java)
                .callRemoveTaskApi(
                    RemoveTaskRequest(
                        "$taskid",
                    )
                )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ response ->
                    dismissLoader()
                    val taskListResponse = response.body()
                    if (response.code() == 401) {
                        responseStatus()
                    }else{
                        if (taskListResponse?.status == 200) {
                            var message = taskListResponse.message
                            taskData.removeAt(arrayPosition)
                            updateRecyclerView(arrayPosition-1)
                        } else {
                            val message = taskListResponse?.message
                            dialogStatus(message!!,10)
                        }
                    }
                }, {
                    dismissLoader()
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                })
        }
    }

}