package com.task.mytodo.activity

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.kaopiz.kprogresshud.KProgressHUD
import com.task.mytodo.R
import io.reactivex.disposables.Disposable

open class BaseActivity : AppCompatActivity() {
    var progressDialog : KProgressHUD? = null
    var disposable: Disposable? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
    }

    fun showLoader(){
        progressDialog = KProgressHUD.create(this)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setCancellable(false)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)
            .show()
    }
    fun dismissLoader() {
        progressDialog!!.dismiss()
    }


    fun isNetworkAvailable():Boolean{
        val runtime : Runtime = Runtime.getRuntime()
        try {
            val ipProcess: Process = runtime.exec("/system/bin/ping -c 1 8.8.8.8")
            val exitValue: Int = ipProcess.waitFor()
            return (exitValue == 0)
        }
        catch (exp:Exception){
            Toast.makeText(this, exp.message, Toast.LENGTH_LONG).show()
        }
        return  false
    }


    fun responseStatus() {
        val builder = androidx.appcompat.app.AlertDialog.Builder(this)
        //set title for alert dialog
        builder.setTitle("Alert")
        //set message for alert dialog
        builder.setMessage("You Need to login again")
        //performing positive action
        builder.setPositiveButton("Ok") { dialogInterface, which ->
            dismissLoader()
            val intent = Intent(this,HomeActivity::class.java)
            startActivity(intent)
        }
//        builder.setNegativeButton("No") { dialogInterface, which ->
//            dialogInterface.dismiss()
//        }
        // Create the AlertDialog
        val alertDialog: androidx.appcompat.app.AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    fun dialogStatus(message: String, status: Int) {
        val builder = androidx.appcompat.app.AlertDialog.Builder(this)
        //set title for alert dialog
        if (status!=0) {
            builder.setTitle("Alert")
        } else {
            builder.setTitle("Success")
        }

        //set message for alert dialog
        builder.setMessage("$message")

        //performing positive action
        builder.setPositiveButton("Ok") { dialogInterface, which ->
            if (status == 0) {
                dismissLoader()
                finish()
            } else {
                dismissLoader()
            }
        }
//        builder.setNegativeButton("No") { dialogInterface, which ->
//            dialogInterface.dismiss()
//        }
        // Create the AlertDialog
        val alertDialog: androidx.appcompat.app.AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }


    protected open fun hideSoftKeyBoard(context: Context) {
        // Check if no view has focus:
        val view = currentFocus
        if (view != null) {
            val imm = (context.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager)!!
            imm!!.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}