package com.task.mytodo.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.task.mytodo.R
import com.task.mytodo.api.ApiInterface
import com.task.mytodo.api.RetrofitApiClient
import com.task.mytodo.databinding.ActivityTaskAddBinding
import com.task.mytodo.model.AddTaskRequest
import com.task.mytodo.model.TaskData
import com.task.mytodo.model.TaskEditRequest
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import java.text.SimpleDateFormat
import java.util.*


class TaskAddActivity : BaseActivity() {
    private lateinit var binding: ActivityTaskAddBinding
    var isUpdate = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTaskAddBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //set current date
        binding.edDate.text = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
        //toolbar
        binding.toolbar.btnBack.setOnClickListener {
            onBackPressed()
        }
        binding.toolbar.activityMainToolbarTitle.text = "Add Task"


        //get extra data
        isUpdate = intent.getBooleanExtra("isUpdate",false)
        if (isUpdate) {
            var data = intent.getSerializableExtra("obj") as TaskData
            if (data != null) {
                binding.toolbar.activityMainToolbarTitle.text = "Edit Task"
                binding.edTask.setText("${data.task}")
                var dateArray = data.date.split("-").toTypedArray()
                var inputDate = "${dateArray[2]}/${dateArray[1]}/${dateArray[0]}"
                binding.edDate.text = "$inputDate"
                binding.edDec.setText("${data.description}")
            }
        }


        binding.datePickerLayout.setOnClickListener {
            datePicker()
        }
        binding.btnSave.setOnClickListener {
            if (binding.edDate.text.toString().isEmpty()) {
                Toast.makeText(this, "Select Date", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            } else if (binding.edTask.text.toString().isEmpty()) {
                Toast.makeText(this, "Add task tittle", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            } else if (binding.edDec.text.toString().isEmpty()) {
                Toast.makeText(this, "Add description", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            } else {
                if (isUpdate) {
                    editTaskAPICall()
                } else {
                    addTaskAPICall()
                }
            }
        }
    }

    private fun datePicker() {
        val c: Calendar = Calendar.getInstance()
        var mYear = c.get(Calendar.YEAR)
        var mMonth = c.get(Calendar.MONTH)
        var mDay = c.get(Calendar.DAY_OF_MONTH)

        try {
            var dateArray = binding.edDate.text.split("/").toTypedArray()
            mYear = dateArray[2].toInt()
            mMonth = dateArray[1].toInt()
            mDay = dateArray[0].toInt()
        } catch (e:Exception) {
        }

        val datePickerDialog = DatePickerDialog(this,
            { view, year, monthOfYear, dayOfMonth ->
                if (monthOfYear+1>=10) {
                    binding.edDate.text = "$dayOfMonth/${monthOfYear + 1}/$year"
                } else {
                    binding.edDate.text = "$dayOfMonth/0${monthOfYear + 1}/$year"
                }
            }, mYear,mMonth-1,mDay
        )
        if (isUpdate) {
//            datePickerDialog.datePicker.maxDate = System.currentTimeMillis() - 1000
        } else {
            datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000
        }
        datePickerDialog.show()
    }

    //Add task api call
    private fun addTaskAPICall() {
        val returnPopUp = isNetworkAvailable()
        hideSoftKeyBoard(this)
        if (!returnPopUp) {
            Toast.makeText(this, "check your internet connection!", Toast.LENGTH_SHORT).show()
            return
        }else{
            var task = binding.edTask.text
            var dec = binding.edDec.text
            var dateArray = binding.edDate.text.split("/").toTypedArray()
            var date = "${dateArray[2]}-${dateArray[1]}-${dateArray[0]}"

            showLoader()
            disposable = RetrofitApiClient.createService("", ApiInterface::class.java)
                .callAddTaskApi(
                    AddTaskRequest(
                        "$task",
                        "$date",
                        "$dec",
                    )
                )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ response ->
                    dismissLoader()
                    val registerResponse = response.body()
                    if (response.code() == 401) {
                        responseStatus()
                    }else{
                        if (registerResponse?.status == 200) {
                            var message = registerResponse.message
                            dialogStatus(message!!,0)
                        } else {
                            val message = registerResponse?.message
                            dialogStatus(message!!,10)
                        }
                    }
                }, {
                    dismissLoader()
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                })
        }
    }

    //Add task api call
    private fun editTaskAPICall() {
        val returnPopUp = isNetworkAvailable()
        hideSoftKeyBoard(this)
        if (!returnPopUp) {
            Toast.makeText(this, "check your internet connection!", Toast.LENGTH_SHORT).show()
            return
        }else{
            var task = binding.edTask.text
            var dec = binding.edDec.text
            var dateArray = binding.edDate.text.split("/").toTypedArray()
            var date = "${dateArray[2]}-${dateArray[1]}-${dateArray[0]}"
//            var date = "2022-02-21"
            var data = intent.getSerializableExtra("obj") as TaskData
            var taskId = data.id
            showLoader()
            disposable = RetrofitApiClient.createService("", ApiInterface::class.java)
                .callUpdateTaskApi(
                    TaskEditRequest(
                        "$taskId",
                        "$task",
                        "$date",
                        "$dec",
                    )
                )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ response ->
                    dismissLoader()
                    val registerResponse = response.body()
                    if (response.code() == 401) {
                        responseStatus()
                    }else{
                        if (registerResponse?.status == 200) {
                            var message = registerResponse.message
                            dialogStatus(message!!,0)
                        } else {
                            val message = registerResponse?.message
                            dialogStatus(message!!,10)
                        }
                    }
                }, {
                    dismissLoader()
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                })
        }
    }

}