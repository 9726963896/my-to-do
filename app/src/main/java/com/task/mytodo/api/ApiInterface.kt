package com.task.mytodo.api


import com.task.mytodo.model.*
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST


interface ApiInterface {
    @POST(ADD_TASK)
    fun callAddTaskApi(@Body body: AddTaskRequest)
    : io.reactivex.Observable<Response<AddTaskResponse>>

    @POST(TASK_LIST)
    fun callAddTaskApi(@Body body: TaskListRequest)
    : io.reactivex.Observable<Response<TaskListResponse>>

    @POST(REMOVE_TASK)
    fun callRemoveTaskApi(@Body body: RemoveTaskRequest)
    : io.reactivex.Observable<Response<RemoveTaskResponse>>

    @POST(UPDATE_TASK)
    fun callUpdateTaskApi(@Body body: TaskEditRequest)
    : io.reactivex.Observable<Response<TaskEditResponse>>
}


