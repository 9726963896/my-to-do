package com.task.mytodo.api


import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitApiClient {

    private val builder = Retrofit.Builder()
        .baseUrl(BASEURL)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())

//    fun <S> createService(serviceClass: Class<S>): S {
//
//        return retrofit().create(serviceClass)
//    }

    fun <S> createService(token:String,serviceClass: Class<S>): S {

        return retrofit().create(serviceClass)
    }

    private fun retrofit(): Retrofit {

        val httpClient = OkHttpClient.Builder()
            .readTimeout(TIMEOUT_READ.toLong(), TimeUnit.MINUTES)
            .connectTimeout(TIMEOUT_CONNECTION.toLong(), TimeUnit.MINUTES)

        httpClient.addInterceptor { chain ->
            val original = chain.request()


            val request = original.newBuilder()
                .method(original.method, original.body).build()

            chain.proceed(request)
        }


        val interceptor = HttpLoggingInterceptor()
        interceptor.level =  HttpLoggingInterceptor.Level.BODY

        httpClient.addInterceptor(interceptor)

        val client = httpClient.build()
        return builder.client(client).build()
    }
}