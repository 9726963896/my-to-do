package com.task.mytodo.api



const val BASEURL = "https://task.fltstaging.com/"
const val centerPart = "api/"

const val ADD_TASK = centerPart + "add-task"
const val TASK_LIST = centerPart + "task-list"
const val REMOVE_TASK = centerPart + "remove-task"
const val UPDATE_TASK = centerPart + "update-task"

