package com.task.mytodo.model

import java.util.HashMap

class AddTaskRequest(
    var task: String,
    var date: String,
    var description: String,
){
    fun getQueryMap(): HashMap<String, String> {
        var hmap: HashMap<String, String> = HashMap()
        hmap.put("task", task)
        hmap.put("date", date)
        hmap.put("description", description)
        return hmap
    }
}

data class AddTaskResponse(
    val data: AddTaskData,
    val message: String,
    val status: Int
)

data class AddTaskData(
    val date: String,
    val description: String,
    val id: Int,
    val task: String
)