package com.task.mytodo.model

import java.io.Serializable
import java.util.HashMap

class TaskListRequest(
    var page: String,
){
    fun getQueryMap(): HashMap<String, String> {
        var hmap: HashMap<String, String> = HashMap()
        hmap.put("page", page)
        return hmap
    }
}

data class TaskListResponse(
    val data: TaskListData,
    val message: String,
    val status: Int
)


data class TaskListData(
    val tasks: ArrayList<TaskData>,
    val total: Int
)

data class TaskData(
    val date: String,
    val description: String,
    val id: Int,
    val task: String
): Serializable