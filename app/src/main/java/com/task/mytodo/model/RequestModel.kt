package com.task.mytodo.model

class RequestModel {
}

class MenuListData(s: String, icDialog: Int) {
    var description: String? = s
    var icon = icDialog
}

class TodoListData(s: String) {
    var description: String? = s
}