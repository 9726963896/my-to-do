package com.task.mytodo.model

import java.util.HashMap


class RemoveTaskRequest(
    var task_id: String,
){
    fun getQueryMap(): HashMap<String, String> {
        var hmap: HashMap<String, String> = HashMap()
        hmap.put("task_id", task_id)
        return hmap
    }
}

data class RemoveTaskResponse(
    val message: String,
    val status: Int
)