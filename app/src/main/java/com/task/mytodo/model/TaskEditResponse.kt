package com.task.mytodo.model

import java.util.HashMap


class TaskEditRequest(
    var task_id: String,
    var task: String,
    var date: String,
    var description: String,
){
    fun getQueryMap(): HashMap<String, String> {
        var hmap: HashMap<String, String> = HashMap()
        hmap.put("task_id", task_id)
        hmap.put("task", task)
        hmap.put("date", date)
        hmap.put("description", description)
        return hmap
    }
}

data class TaskEditResponse(
    val data: TaskEditData,
    val message: String,
    val status: Int
)
data class TaskEditData(
    val date: String,
    val description: String,
    val id: Int,
    val task: String
)