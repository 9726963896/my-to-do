package com.task.mytodo.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.task.mytodo.R
import com.task.mytodo.activity.HomeActivity
import com.task.mytodo.activity.TaskAddActivity
import com.task.mytodo.model.TaskData
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class TodoListAdapter(private val list: ArrayList<TaskData>,
                      val listener: OnClickListener,
                      val homeActivity: HomeActivity)
    : RecyclerView.Adapter<TodoListAdapter.MyView>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyView {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.todo_list_item, parent, false)
        return MyView(itemView)
    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: MyView, position: Int) {
        val listData = list[position]

        holder.txtTaskTitle.text = listData.task

        var dateArray = listData.date.split("-").toTypedArray()
        var inputDate = "${dateArray[2]}/${dateArray[1]}/${dateArray[0]}"
        val format1 = SimpleDateFormat("dd/MM/yyyy")
        val dt1 = format1.parse(inputDate)
        val format2: DateFormat = SimpleDateFormat("EEEE")
        val day: String = format2.format(dt1)
        holder.txtDayDate.text = "$day - $inputDate"

        holder.txtDes.text = listData.description

        holder.editIcon.setOnClickListener {
            val intent = Intent(homeActivity,TaskAddActivity::class.java)
            intent.putExtra("obj",listData)
            intent.putExtra("isUpdate",true)
            homeActivity.startActivity(intent)
        }
        holder.deleteIcon.setOnClickListener {
            listener.onItemClick(position,2,listData.id)
        }


    }

    override fun getItemCount(): Int {
        return list.size
    }


    class MyView(view: View) : RecyclerView.ViewHolder(view) {
        val txtTaskTitle: TextView = itemView.findViewById<View>(R.id.txtTaskTitle) as TextView
        val txtDayDate: TextView = itemView.findViewById<View>(R.id.txtDayDate) as TextView
        val txtDes: TextView = itemView.findViewById<View>(R.id.txtDes) as TextView
        val editIcon: ImageView = itemView.findViewById<View>(R.id.editIcon) as ImageView
        val deleteIcon: ImageView = itemView.findViewById<View>(R.id.deleteIcon) as ImageView
    }


    interface OnClickListener{
        fun onItemClick(position: Int,clickType: Int,taskid: Int)
    }

}